package main

import(
	"gitlab.com/musl/doit/cmd"
)

func main() {
	cmd.Execute()
}