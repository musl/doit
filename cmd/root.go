package cmd

import (
	"path/filepath"
	"log"
	"os"
	"os/exec"

	"github.com/spf13/cobra"
)

func IsExecOwner(mode os.FileMode) bool {
    return mode&0100 != 0
}

func IsExecGroup(mode os.FileMode) bool {
    return mode&0010 != 0
}

func IsExecAny(mode os.FileMode) bool {
    return mode&0111 != 0
}

func luaRunner(path string) (*cobra.Command, error) {
	// TODO: do everythnig that generic runner does, but look for a lua
	// comment at the top of the file and set cobra command metadata
	// based on the contents
	// https://github.com/Shopify/go-lua

	// use https://github.com/Shopify/go-lua

	return nil, nil
}

func jsRunner(path string) (*cobra.Command, error) {
	// TODO: do everythnig that generic runner does, but look for a c-style
	// comment at the top of the file and set cobra command metadata
	// based on the contents

	// use https://github.com/rogchap/v8go

	return nil, nil
}

func scriptRunner(path string) (*cobra.Command, error) {
	// TODO: do everythnig that generic runner does, but look for a bash-style
	// comment at the top of the file and set cobra command metadata
	// based on the contents

	/*
		#!/usr/bin/env bash
		# Short: Check who's logged in
		# Long: This will print out when you run `sub help who`.
		# You can have multiple lines even!
		#
		#    Show off an example indented
		#
		# And maybe start off another one?

		# Provide rbenv completions
		if [ "$1" = "--complete" ]; then
			echo --path
			exec rbenv shims --short
		fi
	*/

	return nil, nil
}

func genericRunner(path string) (*cobra.Command, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	st, err := f.Stat()
	if err != nil {
		return nil, err
	}

	if !st.Mode().IsRegular() || !IsExecAny(st.Mode()){
		log.Println("Ignoring", path, " because it's not an executable regular file")
		return nil, nil
	}

	return &cobra.Command{
		Use: filepath.Base(filepath.Clean(path)),
		Short: filepath.Clean(path),
		Long: "",
		Run: func(cmd *cobra.Command, args []string){
			out, err := exec.Command(path).Output()
			if err != nil {
				// TODO consider error handling for subcommands
				log.Fatal(err)
			}
			log.Println(string(out))
		},
	}, nil
}

var rootCmd = &cobra.Command{
	Use: "",
	Short: "",
	Long: "",
	Run: func(cmd *cobra.Command, args []string) {
		log.Println("done")
	},
}

func Execute() {
	// TODO: configure the path via environment & default paths, like ~/.doit/cmds
	scripts, err := filepath.Glob(filepath.Clean("./cmds/*"))
	if err != nil {
		log.Fatal(err)
	}

	for _, script := range scripts {
		cobraCmd, err := genericRunner(script)
		if cobraCmd == nil && err == nil {
			continue
		}

		if err != nil {
			log.Println(err)
		}

		rootCmd.AddCommand(cobraCmd)
	}

	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
